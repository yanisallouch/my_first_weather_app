# my_first_weather_app

L’objectif est de se familiariser avec les BLoC/Cubits, FutureBuilder, les constructeurs nommés « fromJson », etc.
Ainsi, avec l’application « Météo », je serai amené à créer une application Flutter qui permet d’avoir la météo pour une ville donnée.